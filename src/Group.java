/*
 * Created by Nash Stewart
 */

public class Group
{
	private Student student1;
	private Student student2;
	
	public Group(Student student1, Student student2) 
	{
		this.student1 = student1;
		this.student2 = student2;
	}
	
	public void chatConsole()
	{
		for(int i = 0; i < 5; i++)
		{
			System.out.print(student1.response(i) + "\n" + student2.response(i) + "\n");
		}
	}
	
	public String chat()
	{
		StringBuilder response = new StringBuilder();
		for(int i = 0; i < 5; i++)
		{
			response.append(student1.response(i) + "\n" + student2.response(i) + "\n");
		}
		return response.toString();
	}
	
	/*
	 * GETTERS
	 */
	
	public Student getStudent1() 
	{
		return student1;
	}

	public Student getStudent2() 
	{
		return student2;
	}
	
}
