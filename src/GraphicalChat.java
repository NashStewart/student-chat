
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;
public class GraphicalChat extends JFrame
{
	private static final long serialVersionUID = -7116132329777144721L;
	private JTextArea textAreaDisplay;
	
	// CONSTRUCOR
	public GraphicalChat() 
	{	
		createPanelMain();
		
		setTitle("Graphical Chat - Nash Stewart");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(450, 600);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	// Create main panel
	private void createPanelMain() 
	{
		JPanel panelMain = new JPanel();
		panelMain.setLayout(new FlowLayout());

		textAreaDisplay = new JTextArea(30, 38);
		textAreaDisplay.setLineWrap(true);
		textAreaDisplay.setEditable(false);
		JScrollPane scrollPaneDisplay = new JScrollPane(textAreaDisplay);
		DefaultCaret caret = (DefaultCaret)textAreaDisplay.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		JTextArea textAreaInput = new JTextArea(2, 38);
		textAreaInput.setLineWrap(true);
		textAreaInput.setToolTipText("ctrl + enter to send");
		JScrollPane scrollPaneInput = new JScrollPane(textAreaInput);
	
		JButton btnSend = new JButton("Send");
		btnSend.addMouseListener(new MouseAdapter() 
		{ 
	          public void mousePressed(MouseEvent me) 
	          { 
	        	  appendDisplayText("You: " + textAreaInput.getText());
	        	  textAreaInput.setText("");
	        	  textAreaInput.requestFocus();
	          } 
		});
	    textAreaInput.addKeyListener(new KeyAdapter() 
	    {
	    	public void keyPressed(KeyEvent e) 
	    	{                
	    		if((e.getKeyCode() == KeyEvent.VK_ENTER) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0))
	    		{
	    			appendDisplayText("You: " + textAreaInput.getText() + " (ctlr+enter)");
	    			textAreaInput.setText("");
		        }
		    }        
	    });
   
		panelMain.add(scrollPaneDisplay);
		panelMain.add(scrollPaneInput);
		panelMain.add(btnSend);
		add(panelMain);
	}
	
	public void appendDisplayText(String str)
	{
		textAreaDisplay.append(str + "\n");
	}
}
