/*
 * Created by Nash Stewart
 */

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class Student implements Comparable<Student>
{
	private String firstName;
	private String lastName;
	private Double score;
	private Set<String> responses;
	
	// CONSTRUCTOR
	public Student( String firstName, String lastName )
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.responses = new LinkedHashSet<>();
	}
	
	// Return response at given index
	public String response(int index) 
	{
		ArrayList<String> arrayList = new ArrayList<>();
		for(String s: responses)
		{
			arrayList.add(s);
		}
		return firstName + " " + lastName + ": " + arrayList.get(index);
	}
	
	// Add a response to list of responses
	public void addResponse(String response) 
	{
		responses.add(response);
	}
	
	@Override
	public int compareTo(Student other) {
		String thisName = getFirstName() + "/" + getLastName();
		
		int d = thisName.compareTo(other.getFirstName() + "/" + other.getLastName());
		return d;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}

	/*
	 * GETTERS and SETTERS
	 */
	public String getFirstName() 
	{
		return firstName;
	}

	public void setFirstName(String firstName) 
	{
		if(firstName == "null")
		{
			return;
		}
		
		this.firstName = firstName;
	}

	public String getLastName() 
	{
		return lastName;
	}

	public void setLastName(String lastName) 
	{
		if(lastName == "null")
		{
			return;
		}
		
		this.lastName = lastName;
	}

	public Double getScore() 
	{
		return score;
	}

	public void setScore(Double score) 
	{
		if(score < 0)
		{
			return;
		}
		
		this.score = score;
	}
}
