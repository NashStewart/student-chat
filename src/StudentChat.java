/*
 * Created by Nash Stewart
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class StudentChat
{
	public static void main(String[] args)
	{
		// Create from class and put them in a set
		Set<Student> students = new HashSet<>();
		students.add(new Student("Hayden", "Beadles"));
		students.add(new Student("Gerald", "Carson"));
		
		students.add(new Student("Chad", "Curvin"));
		students.add(new Student("Jayci", "Giles"));
		
		students.add(new Student("Ian", "Hale"));
		students.add(new Student("Joseph", "Hubbard"));
		
		students.add(new Student("Jose", "Jara"));
		students.add(new Student("Jonathan", "Langford"));
		
		students.add(new Student("Trevor", "Marsh"));
		students.add(new Student("Casey", "Mau"));
		
		students.add(new Student("Bryant", "Morrill"));
		students.add(new Student("Mark", "Richardson"));
		
		students.add(new Student("Nash", "Stewart"));
		students.add(new Student("Michael", "Zeller"));
		
		// Auto populate chat responses
		for(Student s: students) 
		{
			s.addResponse("Hi");
			s.addResponse("How are you?");
			s.addResponse("I asked you first.");
			s.addResponse("Real mature!");
			s.addResponse("Whatever...");
		}
		
		// Put all students into groups
		ArrayList<Group> groups = new ArrayList<>();
		boolean second = false;
		Student first = null;
		for(Student s: students)
		{
			if(second)
			{
				groups.add(new Group(first, s));
				second = false;
			} else {
				first = s;
				second = true;
			}
		}
		
		// Sort groups by first/last name of student 1
		Collections.sort(groups, new Comparator<Group>(){
			  public int compare(Group g1, Group g2){
			    return g1.getStudent1().compareTo(g2.getStudent1());
			  }
			});
		
		GraphicalChat graphicalChat = new GraphicalChat();
		
		// Print chats for all groups
		for(Group g: groups)
		{
			graphicalChat.appendDisplayText(g.chat());
		}
	}
}
